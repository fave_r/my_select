/*
** underline.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 19 03:13:30 2014 romaric
** Last update Sun Jan 19 16:31:32 2014 romaric
*/

#include "myselect.h"

void	underline()
{
  char	*str;

  str = tgetstr("us", NULL);
  tputs(str, 1, my_putint);
}

void	stopunderline()
{
  char	*str;

  str = tgetstr("ue", NULL);
  tputs(str, 1, my_putint);
}

void	myclear()
{
  char	*str;

  str = tgetstr("cl", NULL);
  tputs(str, 1, my_putint);
}

void	reverse()
{
  char	*str;

  str = tgetstr("mr", NULL);
  tputs(str, 1, my_putint);
}

void	unreverse()
{
  char	*str;

  str = tgetstr("me", NULL);
  tputs(str, 1, my_putint);
}
