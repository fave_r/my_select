/*
** my_bzero.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 19 02:02:51 2014 romaric
** Last update Sun Jan 19 02:21:58 2014 romaric
*/

#include "myselect.h"

void	my_bzero(char *s, size_t n)
{
  size_t	i;

  i = 0;
  while (i != n)
    {
      s[i] = '\0';
      i++;
    }
}
