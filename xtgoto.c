/*
** xtgoto.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 19 02:23:40 2014 romaric
** Last update Sun Jan 19 18:29:28 2014 romaric
*/

#include "myselect.h"

char	*xtgoto(const char *cap, int col, int row)
{
  char        *tmp;

  tmp = tgoto(cap, col, row);
  if (tmp == NULL)
    {
      my_putstr("\033[31mtgoto fail !\033[0;m", 2);
      resetefin();
      exit(EXIT_FAILURE);
    }
  return (tmp);
}
