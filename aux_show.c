/*
** aux_show.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 19 14:49:06 2014 romaric
** Last update Sun Jan 19 17:23:28 2014 romaric
*/

#include "myselect.h"

void	aux_show(t_list *tmp)
{
    reverse();
    my_putstr(tmp->val, 1);
    unreverse();
    write(1, " ", 1);
}
