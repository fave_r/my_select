##
## Makefile for my_select in /home/blackbird/work/my_select
##
## Made by romaric
## Login   <fave_r@epitech.net>
##
## Started on  Thu Jan 16 10:59:51 2014 romaric
## Last update Sun Jan 19 18:05:53 2014 romaric
##

CC=	gcc

RM=	rm -f

CFLAGS=	-Wextra -Wall -Werror

NAME=	my_select

SRCS=	main.c \
	char.c \
	xfunction.c \
	checkterm.c \
	my_strncmp.c \
	grepkeys.c \
	read.c \
	auxmain.c \
	my_bzero.c \
	xtgoto.c \
	underline.c \
	aux_show.c \
	showlist.c

OBJS=	$(SRCS:.c=.o)

all:	$(NAME)

$(NAME):	$(OBJS)
		$(CC) $(OBJS) -o $(NAME) $(LDFLAGS) -lncurses

clean:
	$(RM) $(OBJS)

fclean:	clean
	$(RM) $(NAME)

re:	fclean all

.PHONY:	all fclean re
