/*
** auxmain.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sat Jan 18 19:56:56 2014 romaric
** Last update Sun Jan 19 18:26:39 2014 romaric
*/

#include "myselect.h"

void	auxmain(int ac, char **env)
{
  char	*term;

  if (ac == 1)
    exit(EXIT_FAILURE);
  if (*env != NULL)
    {
      term = checterm(env);
      xtgetent(term);
    }
  else
    exit(EXIT_FAILURE);
  signal(SIGINT, handle_signal);
}

void    handle_signal()
{
}
