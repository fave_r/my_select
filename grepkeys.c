/*
** grepkeys.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sat Jan 18 19:22:20 2014 romaric
** Last update Sun Jan 19 18:58:01 2014 romaric
*/

#include "myselect.h"

char	*grepup(void)
{
  char	*res;

  res = tgetstr("ks", NULL);
  tputs(xtgoto(res, 0, 0), 1, &my_putint);
  res = tgetstr("ku", NULL);
  return (res);
}

char	*grepdown(void)
{
  char  *res;

  res = tgetstr("ks", NULL);
  tputs(xtgoto(res, 0, 0), 1, &my_putint);
  res = tgetstr("kd", NULL);
  return (res);
}

char    *grepright(void)
{
  char  *res;

  res = tgetstr("ks", NULL);
  tputs(xtgoto(res, 0, 0), 1, &my_putint);
  res = tgetstr("kr", NULL);
  return (res);
}

char    *grepleft(void)
{
  char  *res;

  res = tgetstr("ks", NULL);
  tputs(xtgoto(res, 0, 0), 1, &my_putint);
  res = tgetstr("kl", NULL);
  return (res);
}

void	grepenter(char *buffer, t_list *racine)
{
  if (buffer[0] == '\n')
    {
      myclear();
      showlist(racine);
      resetefin();
      exit(EXIT_SUCCESS);
    }
}
