/*
** my_strncmp.c for minishell in /home/blackbird/work/minishell/1
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Dec 22 17:26:30 2013 romaric
** Last update Sat Jan 18 14:21:42 2014 romaric
*/

#include "myselect.h"

int	my_strncmp(char *s1, char *s2, int n)
{
  int	i;

  i = 0;
  while (i < n)
    {
      if (s1[i] == '\0' && s2[i] == '\0')
	return (0);
      if (s1[i] < s2[i] && i < n)
	return (-1);
      if (s1[i] > s2[i] && i < n)
	return (1);
      i++;
    }
  return (0);
}
