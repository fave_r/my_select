/*
** main.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Tue Jan 14 13:56:32 2014 romaric
** Last update Sun Jan 19 19:00:26 2014 romaric
*/

#include "myselect.h"

int	main(int ac, char **av, char **env)
{
  struct termios	t;
  t_list		*p;
  t_list		*racine;
  int			i;

  auxmain(ac, env);
  racine = newlist(av[1]);
  p = racine;
  i = ac;
  while (i > 1)
    {
      adnext(p, av[i]);
      i--;
    }
  myclear();
  show_list(racine);
  xtcgetattr(0, &t);
  t.c_lflag &= ~ICANON;
  t.c_lflag &= ~ECHO;
  xtcsetattr (0, 0, &t);
  readbuf(racine);
  resetefin();
  return (0);
}

void	show_list(t_list *ptr)
{
  t_list	*tmp;

  tmp = ptr;
  while (tmp->suiv != ptr)
    {
      if (tmp->isunder == 1)
	{
	  underline();
	  my_putstr(tmp->val, 1);
	  stopunderline();
	  write(1, " ", 1);
	  tmp = tmp->suiv;
	}
      else if (tmp->isunder == 0 && tmp->isselect == 0)
	{
	  my_putstr(tmp->val, 1);
	  write(1, " ", 1);
	  tmp = tmp->suiv;
	}
      else if (tmp->isselect == 1)
	{
	  aux_show(tmp);
	  tmp = tmp->suiv;
	}
    }
}

t_list		*newlist(char *str)
{
  t_list	*racine;

  racine = xmalloc(sizeof(*racine));
  if (racine != NULL)
    {
      racine->val = str;
      racine->isunder = 1;
      racine->isselect = 0;
      racine->prec = racine;
      racine->suiv = racine;
    }
  return (racine);
}

void		adnext(t_list *element, char *val)
{
  t_list	*new_element;

  new_element = xmalloc(sizeof (*new_element));
  if (new_element != NULL)
    {
      new_element->val = val;
      new_element->isunder = 0;
      new_element->isselect = 0;
      new_element->prec = element;
      new_element->suiv = element->suiv;
      element->suiv->prec = new_element;
      element->suiv = new_element;
    }
}
