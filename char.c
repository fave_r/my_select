/*
** char.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Tue Jan 14 13:56:57 2014 romaric
** Last update Sun Jan 19 03:01:22 2014 romaric
*/

#include "myselect.h"

int     my_strlen(char *str)
{
  int   x;

  x = 0;
  if (str != NULL)
    {
      while (str[x] != '\0' && str[x] != '\n')
	x++;
      return (x + 1);
    }
  return (0);
}

int     my_putstr(char *str, int op)
{
  return (write(op, str, my_strlen(str)));
}

int	my_putint(int	i)
{
  return (write(1, &i, 1));
}
