/*
** myselect.h for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sat Jan 18 19:34:25 2014 romaric
** Last update Sun Jan 19 19:01:12 2014 romaric
*/

#ifndef __MYSELECT__
#define __MYSELECT__

#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <termios.h>
#include <term.h>
#include <curses.h>
#include <sys/wait.h>

typedef struct		s_list
{
  char			*val;
  int			isunder;
  int			isselect;
  struct s_list*	prec;
  struct s_list*	suiv;
}			t_list;

int     my_strlen(char *str);
int     my_putstr(char *str, int op);
int     xopen(const char *pathname, int flags);
ssize_t xread(int fd, void *buf, size_t count);
void    *xmalloc(size_t n);
t_list	*newlist(char *str);
void    adnext(t_list* element, char *val);
void    show_list(t_list *ptr);
char    *rmterm(char *dest, char * src);
char    *termcpy(char **env, int i);
int      checkenvterm(char **env);
char	*checterm(char **env);
int     my_strncmp(char *s1, char *s2, int n);
int     my_putint(int    i);
char    *grepup(void);
char    *grepdown(void);
char    *grepright(void);
char    *grepleft(void);
void    readbuf(t_list *racine);
t_list	*greple(char *buffer, t_list *p, t_list *racine);
void    auxmain(int ac, char **env);
void    xtgetent(char *term);
void	xtcgetattr(int fd, void *t);
void	xtcsetattr(int fd, int opt, void *t);
void    my_bzero(char *s, size_t n);
char	*xtgoto(const char *cap, int col, int row);
void    underline();
void    stopunderline();
void	myclear();
t_list	*grepri(char *buffer, t_list *p, t_list *racine);
t_list	*grepsp(char *buffer, t_list *p, t_list *racine);
void    reverse();
void    unreverse();
void	aux_show(t_list *tmp);
void    showlist(t_list *ptr);
void    grepenter(char *buffer, t_list *racine);
void	grepesc(char *buffer, int len);
void    handle_signal();
int     resetefin();

#endif
