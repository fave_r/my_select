/*
** showlist.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sun Jan 19 17:35:14 2014 romaric
** Last update Sun Jan 19 19:47:46 2014 romaric
*/

#include "myselect.h"

void	showlist(t_list *ptr)
{
  t_list        *tmp;

  tmp = ptr;
  while (tmp->suiv != ptr)
    {
      if (tmp->isselect == 1)
	{
	  my_putstr(tmp->val, 1);
          write(1, " ", 1);
          tmp = tmp->suiv;
        }
      else
	tmp = tmp->suiv;
    }
}

void	grepesc(char *buffer, int len)
{
  if (buffer[0] == 27 && len == 1)
    {
      resetefin();
      exit(EXIT_FAILURE);
    }
}

int	resetefin()
{
  struct termios	term;

  if (tcgetattr(0, &term) == -1)
    return (-1);
  term.c_lflag = (ICANON | ECHO);
  if (tcsetattr(0, 0, &term) == -1)
    return (-1);
  return (0);
}
