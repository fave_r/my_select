/*
** checkterm.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sat Jan 18 13:15:05 2014 romaric
** Last update Sat Jan 18 22:03:17 2014 romaric
*/

#include "myselect.h"

int     checkenv(char **env)
{
  int   i;

  i = 0;
  while (env[i] != NULL)
    i++;
  return (i);
}

char	*checterm(char **env)
{
  int	i;
  char	*term;

i = checkenvterm(env);
term = termcpy(env, i);
 return (term);
}

int      checkenvterm(char **env)
{
  int   i;
  int   x;

  i = 0;
  x = 1;
  while (i < checkenv(env))
    {
      x = my_strncmp("TERM=", env[i], 5);
      if (x == 0)
        return (i);
      i++;
    }
  return (0);
}

char    *termcpy(char **env, int i)
{
  char  *term;

  term = xmalloc(my_strlen(env[i]) * sizeof(char) - 5);
  rmterm(term, env[i]);
  return (term);
}

char    *rmterm(char *dest, char * src)
{
  int   i;
  int   x;

  i = 5;
  x = 0;
  while (src[i] != '\0')
    {
      dest[x] = src[i];
      i++;
      x++;
    }
  return (dest);
}
