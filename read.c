/*
** read.c for my_select in /home/blackbird/work/my_select
**
** Made by romaric
** Login   <fave_r@epitech.net>
**
** Started on  Sat Jan 18 19:45:14 2014 romaric
** Last update Sun Jan 19 17:53:03 2014 romaric
*/

#include "myselect.h"

void	readbuf(t_list *racine)
{
  char	buffer[4096];
  t_list	*save;
  int		len;

  save = racine;
  while ((len = xread(0, buffer, 4096)))
    {
      if (grepup() != NULL)
        {
	  if (strcmp(grepup(), buffer) == 0)
            my_putstr("UP\n", 1);
        }
      if (grepdown() != NULL)
	{
          if (strcmp(grepdown(), buffer) == 0)
            my_putstr("DOWN\n", 1);
        }
      save = grepsp(buffer, save, racine);
      save = grepri(buffer, save, racine);
      save = greple(buffer, save, racine);
      grepenter(buffer, racine);
      grepesc(buffer, len);
      my_bzero(buffer, 4096);
    }
}

t_list	*grepri(char *buffer, t_list *p, t_list *racine)
{
  if (grepright() != NULL)
    {
      if (strcmp(grepright(), buffer) == 0)
        {
	  myclear();
	  p->isunder = 0;
	  p = p->suiv;
	  p->isunder = 1;
	  show_list(racine);
	  p->isunder = 0;
	  return (p);
	}
    }
  return (p);
}

t_list	*greple(char *buffer, t_list *p, t_list *racine)
{
  if (grepleft() != NULL)
    {
      if (strcmp(grepleft(), buffer) == 0)
	{
          myclear();
          p->isunder = 0;
	  p = p->prec;
	  p->isunder = 1;
	  show_list(racine);
	  p->isunder = 0;
	  return (p);
	}
    }
  return (p);
}

t_list	*grepsp(char *buffer, t_list *p, t_list *racine)
{
  if (buffer[0] == 32)
    {
      myclear();
      if (p->isselect == 1)
	p->isselect = 0;
      else
	p->isselect = 1;
      p->suiv->isunder = 1;
      p->isunder = 0;
      p = p->suiv;
      show_list(racine);
      return (p);
    }
  return (p);
}
